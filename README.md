### Сборка
Необходимо выполнить команду:
```sh
$ ./gradlew clean build
```

Исполняемый файл: *./build/libs/capacity.jar*

### Запуск
Для запуска необходимо выполнить команду:

```sh
$ java <параметры> -jar capacity.jar
```

Параметры могут быть:
```sh 
-Xmx1024m -Xms256m
```