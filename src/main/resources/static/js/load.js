function loadModule(module, callback) {
    var script = document.createElement('script');
    script.onload = function () {
        console.info(module.name + " --- loaded");
        if (module.next !== null) loadModule(module.next)
    };
    script.src = module.src;
    document.head.appendChild(script);
    if (callback !== undefined) {
        callback();
    }
}

var module = {

    name: "Lodash loaded",
    src: "js/lodash.js",
    next: {
        name: "Values JS",
        src: "js/values.js",
        next: {
            name: "Capacity JS",
            src: "js/capacity.js",
            next: null
        }
    }

};

loadModule(module);

