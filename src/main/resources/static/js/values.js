function draw(props) {
    var capacityTable = document.getElementById("capacityValues");
    props.forEach(drawLine);

    function drawLine(prop) {
        var tr = document.createElement("tr");

        var tdName = drawTd(prop.name, "textBold");
        var tdValue = drawTd(prop.value, "alignRight");

        tr.appendChild(tdName);
        tr.appendChild(tdValue);
        capacityTable.appendChild(tr);
    }

    function drawTd(value, classes) {
        var td = document.createElement("td");
        td.className += classes;
        td.setAttribute("nowrap", "nowrap");
        td.innerText = value;
        return td;
    }
}

var props = [
    {
        name: "Местное время ДО",
        value: "11:00:15"
    },
    {
        name: "Среднее время ожидания(физические лица)",
        value: "2:30"
    },
    {
        name: "Текущее время ожидания(физические лица)",
        value: "0:0"
    },
    {
        name: "Человек в очереди(физические лица)",
        value: "2:30"
    },
    {
        name: "Среднее время ожидания(юридические лица)",
        value: "0:0"
    },
    {
        name: "Текущее время ожидания(юридические лица)",
        value: "0:0"
    },
    {
        name: "Человек в очереди(юридические лица)",
        value: "0:30"
    }
];

draw(props);