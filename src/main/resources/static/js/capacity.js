function draw(data) {
    var capacityTable = document.getElementById("capacityTable");

    drawHeaders(capacityTable, data.headers);
    data.days.forEach(
        function (day) {
            var row = drawRow(capacityTable, day.name);

            day.values.forEach(function (time) {
                drawColorCell(row, time.values)
            })
        }
    );

    function drawHeaders(table, headers) {
        var row = drawRow(table, "");
        headers.forEach(function (header) {
            drawCell(row, header.toString(), "")
        })
    }

    function drawRow(table, value) {
        var row = document.createElement("tr");
        table.appendChild(row);
        drawCell(row, value, "");
        return row;
    }

    function drawCell(row, value, classes) {
        var cell = document.createElement("td");

        if (typeof value === 'string') {
            cell.innerText = value;
        } else {
            if (_.isArray(value)) {
                value.forEach(function(obj) {cell.appendChild(obj)})
            } else {
                cell.appendChild(value);
            }

        }
        row.appendChild(cell);
        cell.className += classes;
        return cell;
    }

    function drawColorCell(row, values) {

        drawCell(row, _.map(values, drawColorDiv), "");
    }

    function drawColorDiv(value) {
        var div = document.createElement("span");
        div.innerText = " ";
        div.className += "colorSpan";

        if (value === 1) {
            div.className += " green";
        }
        else if (value === 2) {
            div.className += " yellow";
        }
        else if (value === 3) {
            div.className += " red";
        } else {
            div.className += " noneCapacity";
        }

        return div;
    }
};



var values = {
    headers: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
    days: [
        {
            name: "Пн",
            values: [
                {
                    hour: 10,
                    values: [1, 1]
                },
                {
                    hour: 11,
                    values: [2, 1]
                },
                {
                    hour: 12,
                    values: [2, 2]
                },
                {
                    hour: 13,
                    values: [3, 3]
                },
                {
                    hour: 14,
                    values: [3, 3]
                },
                {
                    hour: 15,
                    values: [3, 3]
                },
                {
                    hour: 16,
                    values: [3, 3]
                },
                {
                    hour: 17,
                    values: [3, 3]
                },
                {
                    hour: 18,
                    values: [3, 3]
                },
                {
                    hour: 19,
                    values: [3, 3]
                }
            ]
        },
        {
            name: "Вт",
            values: [
                {
                    hour: 10,
                    values: [1, 1]
                },
                {
                    hour: 11,
                    values: [2, 1]
                },
                {
                    hour: 12,
                    values: [2, 2]
                },
                {
                    hour: 13,
                    values: [3, 3]
                },
                {
                    hour: 14,
                    values: [3, 3]
                },
                {
                    hour: 15,
                    values: [3, 3]
                },
                {
                    hour: 16,
                    values: [3, 3]
                },
                {
                    hour: 17,
                    values: [3, 3]
                },
                {
                    hour: 18,
                    values: [3, 3]
                }
                ,
                {
                    hour: 19,
                    values: [3, 3]
                }
            ]
        },
        {
            name: "Ср",
            values: [
                {
                    hour: 10,
                    values: [1, 1]
                },
                {
                    hour: 11,
                    values: [2, 1]
                },
                {
                    hour: 12,
                    values: [2, 2]
                },
                {
                    hour: 13,
                    values: [3, 3]
                },
                {
                    hour: 14,
                    values: [3, 3]
                },
                {
                    hour: 15,
                    values: [3, 3]
                },
                {
                    hour: 16,
                    values: [3, 3]
                },
                {
                    hour: 17,
                    values: [3, 3]
                },
                {
                    hour: 18,
                    values: [3, 3]
                }
                ,
                {
                    hour: 19,
                    values: [3, 3]
                }
            ]
        },
        {
            name: "Чт",
            values: [
                {
                    hour: 10,
                    values: [1, 1]
                },
                {
                    hour: 11,
                    values: [2, 1]
                },
                {
                    hour: 12,
                    values: [2, 2]
                },
                {
                    hour: 13,
                    values: [3, 3]
                },
                {
                    hour: 14,
                    values: [1, 2]
                },
                {
                    hour: 15,
                    values: [2, 2]
                },
                {
                    hour: 16,
                    values: [1, 1]
                },
                {
                    hour: 17,
                    values: [3, 3]
                },
                {
                    hour: 18,
                    values: [3, 3]
                }
                ,
                {
                    hour: 19,
                    values: [3, 3]
                }
            ]
        },
        {
            name: "Пт",
            values: [
                {
                    hour: 10,
                    values: [1, 1]
                },
                {
                    hour: 11,
                    values: [2, 1]
                },
                {
                    hour: 12,
                    values: [2, 2]
                },
                {
                    hour: 13,
                    values: [1, 1]
                },
                {
                    hour: 14,
                    values: [1, 1]
                },
                {
                    hour: 15,
                    values: [1, 1]
                },
                {
                    hour: 16,
                    values: [1, 1]
                },
                {
                    hour: 17,
                    values: [1, 1]
                },
                {
                    hour: 18,
                    values: [1, 1]
                }
                ,
                {
                    hour: 19,
                    values: [1, 1]
                }
            ]
        }
    ]
};

draw(values);